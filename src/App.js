import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

export const App = () => {

    return(
        <Provider store={stores}>
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Main}/>
                </Switch>
            </BrowserRouter>
        </Provider>
    );
}

